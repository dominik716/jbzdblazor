﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JbzdBlazor.Utils
{
    public static class StringUtils
    {
        public static string GetStringBetween(String text, String start, String end)
        {
            int indexStart = text.IndexOf(start);
            if (indexStart < 0) return null;
            //int indexEnd = text.IndexOf(start);
            int p1 = indexStart + start.Length;
            int p2 = text.IndexOf(end, p1);
            if (p2 < 0) return null;
            if (end == "") return (text.Substring(p1));
            else return text.Substring(p1, p2 - p1);
        }

        public static List<string> GetStringBetweenAll(string STR, string FirstString, string LastString)
        {
            string dane;
            List<string> listOfStrings = new List<string>();
            do
            {
                 dane = GetStringBetween(STR, FirstString, LastString);
                 if (dane == null) break;
                 STR = STR.Replace( FirstString + dane + LastString, "");
                 listOfStrings.Add(dane);
            } while (dane != null);

            return listOfStrings;
        }

        /// <summary>
        /// Usuwa z podanego Ciągu, tekst pomiędzy parametrami włącznie z parametrami
        /// </summary>
        /// <param name="STR"></param>
        /// <param name="FirstString"></param>
        /// <param name="LastString"></param>
        /// <returns></returns>
        public static string RemoveStringBetweenIncludingParam(string STR, string FirstString,
            string LastString)
        {
            string between = GetStringBetween(STR, FirstString, LastString);
            return STR.Replace(FirstString + between + LastString, "");
        }

    }


}
