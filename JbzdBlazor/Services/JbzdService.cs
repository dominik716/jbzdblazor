﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using JbzdBlazor.Models;
using JbzdBlazor.Utils;

namespace JbzdBlazor.Services
{
    public class JbzdService
    {
        private readonly string _JbzdURL = @"https://jbzd.com.pl/str/";

        public async Task<List<Article>> GetArticlesOnPageAsync(int numberOfPage = 1)
        {
            var articles = new List<Article>();
            numberOfPage = numberOfPage < 1 ? 1 : numberOfPage;
            using (var client = new HttpClient())
            {
                string webpageContent = await client.GetStringAsync(_JbzdURL + numberOfPage);
                var articlesRaw = Utils.StringUtils.GetStringBetweenAll(webpageContent, "<article class=\"article\"", "</article>");
                foreach (var articleRaw in articlesRaw)
                {
                    if(articleRaw.Contains("href=\"https://posty.pl/")) continue;
                    string title = StringUtils.GetStringBetween(articleRaw, "<h3 class=\"article-title\">", "<div");
                    string url = StringUtils.GetStringBetween(title, "<a href=\"", "\" >");
                    int ArticleId = int.Parse(StringUtils.GetStringBetween(url, "/obr/", "/"));
                    title = StringUtils.RemoveStringBetweenIncludingParam(title, "<a href=\"", "\" >");
                    title = title.Replace("</a>", "").Trim();
                    
                    string articleRawWithoutAvatar = StringUtils.RemoveStringBetweenIncludingParam(articleRaw,
                        "<a href=\"#\" class=\"article-avatar\">", "</a>"); //Usuwa Obrazek z avatarem
                    string image = StringUtils.GetStringBetween(articleRawWithoutAvatar, "<img src=\"", "\" class");
                    if(string.IsNullOrEmpty(image)) continue;
                    articles.Add(new Article(){
                        ImageUrl = image, Title = title, Url = url, IdFromUrl = ArticleId
                    });
                }
            }

            return articles;
        }
    }


}
