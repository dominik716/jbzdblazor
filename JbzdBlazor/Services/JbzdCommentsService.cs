﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using JbzdBlazor.Models;
using JbzdBlazor.Models.JbzdComments;

namespace JbzdBlazor.Services
{
    public class JbzdCommentsService
    {
        private string cookie = "";
        private readonly string _JbzdURL = @"https://jbzd.com.pl/comment/content/listing/";

        public async Task<List<Comment>> getCommentsAsync(Article article)
        {
            List<Comment> commentsList = new List<Comment>();
            string pageContent = await getCommentContentFromWebAsync(article);
            var jbzdCommentObject = mapContentToObject(pageContent);

            if (jbzdCommentObject.total_count == 0)
            {
                return commentsList;
            }

            foreach (var commentRaw in jbzdCommentObject.pagination.data)
            {
                List<Comment> subComments = new List<Comment>();

                foreach (var subCommentRaw in commentRaw.children)
                {
                    var subcomment = new Comment()
                    {
                        CommentContent = subCommentRaw.comment,
                        Score = subCommentRaw.score,
                        UserName = subCommentRaw.user.name,
                        UserAvatarURL = subCommentRaw.user.avatar_url.large
                    };
                    subComments.Add(subcomment);
                }

                Comment comment = new Comment()
                {
                    CommentContent = commentRaw.comment,
                    Score = commentRaw.score,
                    UserName = commentRaw.user.name,
                    UserAvatarURL = commentRaw.user.avatar_url.large,
                    Children = subComments
                };
                commentsList.Add(comment);
            }

            return commentsList;

        }

        private JbzdCommentRoot mapContentToObject(string content)
        {
            return System.Text.Json.JsonSerializer.Deserialize<JbzdCommentRoot>(content);
        }

        private async Task<string> getCommentContentFromWebAsync(Article article)
        {
            string pageContent;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("x-requested-with", "XMLHttpRequest");
                pageContent =
                    await client.GetStringAsync(
                        _JbzdURL + article.IdFromUrl + "?page=1&per_page=100&sort=best");
            }

            return pageContent;
        }
    }
}
