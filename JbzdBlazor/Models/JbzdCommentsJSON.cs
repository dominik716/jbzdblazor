﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JbzdBlazor.Models.JbzdComments
{
    public class IsVoted
    {
        public bool status { get; set; }
        public object forr { get; set; }
    }

    public class Commentable
    {
        public int id { get; set; }
        public int user_id { get; set; }
        //public int category_id { get; set; }
        public string state { get; set; }
        public object state_final { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string published_at { get; set; }
        public int sequence { get; set; }
        public bool mature { get; set; }
        public bool mature_api { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public object description { get; set; }
        public string pattern { get; set; }
        public string slug { get; set; }
        public object redirect_url { get; set; }
        public int plus { get; set; }
        public int share { get; set; }
        public bool indexed { get; set; }
        public bool mca { get; set; }
        public bool mero { get; set; }
        public int version { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object deleted_at { get; set; }
        public string url { get; set; }
        public string url_fp { get; set; }
        public int plus_real { get; set; }
        public int minus_real { get; set; }
    }

    public class AvatarUrl
    {
        public string small { get; set; }
        public string medium { get; set; }
        public string large { get; set; }
        public string original { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public string avatar { get; set; }
        public int gender { get; set; }
        public int score { get; set; }
        public int rank { get; set; }
        public string created_at { get; set; }
        public AvatarUrl avatar_url { get; set; }
        public string url { get; set; }
        public int plus_real { get; set; }
        public int minus_real { get; set; }
    }

    public class Child
    {
        public int id { get; set; }
        public int commentable_id { get; set; }
        public int user_id { get; set; }
        public int parent_id { get; set; }
        public int? parent_real_id { get; set; }
        public string comment { get; set; }
        public int score { get; set; }
        public int plus { get; set; }
        public int minus { get; set; }
        public bool removed { get; set; }
        public int nuked { get; set; }
        public int marked { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public IsVoted is_voted { get; set; }
        public string commentable_url { get; set; }
        public int plus_real { get; set; }
        public int minus_real { get; set; }
        public User user { get; set; }
    }

    public class Datum
    {
        public int id { get; set; }
        public int commentable_id { get; set; }
        public int user_id { get; set; }
        public object parent_id { get; set; }
        public object parent_real_id { get; set; }
        public string comment { get; set; }
        public int score { get; set; }
        public int plus { get; set; }
        public int minus { get; set; }
        public bool removed { get; set; }
        public int nuked { get; set; }
        public int marked { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public IsVoted is_voted { get; set; }
        public string commentable_url { get; set; }
        public int plus_real { get; set; }
        public int minus_real { get; set; }
        public Commentable commentable { get; set; }
        public User user { get; set; }
        public IList<Child> children { get; set; }
    }

    public class Pagination
    {
        public int current_page { get; set; }
        public IList<Datum> data { get; set; }
        public string first_page_url { get; set; }
        public int from { get; set; }
        public int last_page { get; set; }
        public string last_page_url { get; set; }
        public object next_page_url { get; set; }
        public string path { get; set; }
        public string per_page { get; set; }
        public object prev_page_url { get; set; }
        public int to { get; set; }
        public int total { get; set; }
    }

    public class JbzdCommentRoot
    {
        public string status { get; set; }
        public Pagination pagination { get; set; }
        public int total_count { get; set; }
    }


}
