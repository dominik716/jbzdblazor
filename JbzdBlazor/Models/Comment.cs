﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JbzdBlazor.Models
{
    public class Comment
    {
        public string UserName { get; set; }
        public string UserAvatarURL { get; set; }
        public int Score { get; set; }
        public string CommentContent { get; set; }
        public List<Comment> Children { get; set; }
    }
}
